#!/bin/python
import random

class package():
    # Package Class for each package
    def __init__(self,size,lvl,owner,location):
        self.size = size
        self.lvl = lvl
        self.owner = owner
        self.location = location

    def print_info(self):
        # Prints All info of each package
        print(f"""=======\nPackage Info\n\nDifficulty Level: {self.lvl} \nOwner: {self.owner}\nLocation: {self.location}\n=======""")

def generate_package():
    # Creates package
    pack_size = random.randint(1,9)
    lvl = random.randint(1,100)
    owner = get_rank() + " "+ get_name() + " "+ get_name()
    location = get_name() + " " + get_location()
    pack = package(pack_size,lvl,owner,location)
    pack.print_info()

### Generate Random Info ###

def get_rank():
    # Randomizes a Rank
    ranks = ["Admiral","Mr","Mrs","Captain","Sergeant","Private","Dr","Sir","Lord" , "Miss","General","President","Duke","Madam"]
    rank_num = random.randint(0,len(ranks)-1)
    return ranks[rank_num]

def get_location():
    # Randomizes location
    location_owner = get_name()
    locations = ["Train Station","Restaurant","Hospital","Military Base","House","Estate","Mansion","Hotel","Airport","Bakery"]
    loc_num = random.randint(0,len(locations)-1)
    return locations[loc_num]

def get_word():
    # Randomizes a word
    consonant = ['b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','y','z']
    vowel = ['a','e','i','o','u']
    num_syllable = random.randint(1,6)
    word = ""
    for i in range(num_syllable):
        has_ending = random.randint(0,3)
        has_double_vow = random.randint(0,7)
        has_double_const = random.randint(0,7)
        if has_ending == 0:
            end = consonant[random.randint(0,len(consonant)-1)]
        else:
            end = ""
        const = consonant[random.randint(0,len(consonant)-1)]
        vow = vowel[random.randint(0,len(vowel)-1)]
        if has_double_vow == 1:
            vow += vow
        if has_double_const == 1:
            const += consonant[random.randint(0,len(consonant)-1)]
        word += const+vow+end
    return word

def get_name():
    # Randomizes a name
    name = get_word() 
    return name[0].upper() + name[1:]

### Main ###

if __name__=='__main__':
    get_package()


